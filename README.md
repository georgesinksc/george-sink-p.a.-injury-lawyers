We are a personal injury law firm with locations all across South Carolina and Georgia. We are proud to represent car accident and injured victims who are not receiving their due compensation from the big insurance companies. George Sink and his team of attorneys will fight for you.

Address: 113 East Washington Street, Suite A, Walterboro, SC 29488, USA

Phone: 843-306-0009